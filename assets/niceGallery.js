$(document).ready(init);

let images;
let currentImageIndex = 0;

// Initializing controllers and click-events
function init()
{
	$(document).keydown(function(event){keyPress(event)});

	let galleryRootEl = $("#nice-gallery");
	galleryRootEl.prepend(`
	<div id="nice-gallery__lightbox">
		<div id="nice-gallery__lightbox__bg"></div>
		<div id="nice-gallery__lightbox__img-container">
			<img id="nice-gallery__lightbox__image" src="">
		</div>
		<div id="nice-gallery__lightbox__controllers">
			<div><i class="fa fa-chevron-left prev"></i></div>
			<div><i class="fa fa-chevron-right next"></i></div>
		</div>
	</div>`);

	setImages();

	$("#nice-gallery__lightbox__bg").click(function(){setLightboxActive(false)});
	$("#nice-gallery__lightbox__image").click(function(){stepImages(1)});
	$("#nice-gallery__lightbox__controllers .prev").click(function(){stepImages(-1)});
	$("#nice-gallery__lightbox__controllers .next").click(function(){stepImages(1)});
}

// This method can be called if images are inserted after initialization
function setImages()
{
	images = $("#nice-gallery__images img");
	$(images).off("click");
	$(images).click(function(){setLightboxImage(this)});
}

// Used for toggling lightbox on and off
function setLightboxActive(active)
{
	console.log("setLightboxActive", active);
	
	$("#nice-gallery__lightbox").css(
		"display", active ? "block" : "none");

	if (active)
		$("body").addClass("no-scroll");
	else
		$("body").removeClass("no-scroll");
}

// Stepping though images
function stepImages(step)
{
	console.log(step);
	currentImageIndex += step;

	if (currentImageIndex < 0)
		currentImageIndex = images.length - 1;
	else if (currentImageIndex >= images.length)
		currentImageIndex = 0;

	setLightboxImage($("#nice-gallery__images img:eq("+currentImageIndex+")"));
}

// Setting the current image in the lightbox
function setLightboxImage(imgElement)
{
	console.log(imgElement);
	currentImageIndex = $(imgElement).index();
	$("#nice-gallery__lightbox__image").attr("src", $(imgElement).attr("src"));
	setLightboxActive(true);
}

// Key-controllers
function keyPress(event)
{
	switch (event.key) {
		case "Escape":
			setLightboxActive(false);
			break;
		case "ArrowLeft":
			stepImages(-1);
			break;
		case "ArrowRight":
			stepImages (1);
			break;
	}
}
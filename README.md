# NiceGallery

NiceGallery is an easy implementable gallery with a lightbox viewer.

## Implementation

NiceGallery requires you to import some stylesheets and scripts, font-awesome, niceGallery.css, jquery, and niceGallery.js.

This is the base html needed to initialize NiceGallery. Full html-file with example included in the asset.

```html
<head>
	<link href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/niceGallery.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="assets/niceGallery.js" defer></script>
</head>
<body>
	<div id="nice-gallery">
		<div id="nice-gallery__images">
		<!-- Example images -->
			<img src="https://picsum.photos/800/600/?image=100">
			<img src="https://picsum.photos/800/600/?image=200">
			<img src="https://picsum.photos/800/600/?image=300">
			<img src="https://picsum.photos/800/600/?image=400">
			<img src="https://picsum.photos/800/600/?image=500">
			<img src="https://picsum.photos/800/600/?image=600">
		</div>
	</div>
</body>
```

## License
[MIT](https://choosealicense.com/licenses/mit/)